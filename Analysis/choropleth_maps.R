# Program: choropleth_maps.R ####
# Description: Create choropleth maps for market structures and smart meter diffusion
# Programmer: Derek Ryan Strong
# Date: 2017-03-16

# Clear environment

rm(list = ls())

# Load packages

library(plyr)
library(choroplethr)
library(choroplethrMaps)
library(ggplot2)
library(grid)
library(gridExtra)

# Import datasets

state_reg <- read.csv("./Data/State_Regulation/state_reg.csv", stringsAsFactors = FALSE)
meters <- read.csv("./Data/EIA-861/Modified/Cleaned/meters_desc.csv", stringsAsFactors = FALSE)


# Create choropleth map for restructuring status ####

# Prepare data for use by choroplethr package

# Select variables and delete last row
state_reg2 <- state_reg[ , c("state", "wholesale_comp", "customer_choice2")]
state_reg2 <- state_reg2[-52, ]

# Create variable for restructuring status
state_reg2$value <- ifelse(state_reg2$customer_choice2 == "Y" & state_reg2$wholesale_comp == "Y", "D",
                           ifelse(state_reg2$customer_choice2 == "Y", "C",
                           ifelse(state_reg2$wholesale_comp == "Y", "B", "A")))
# = A if not restructured
# = B if wholesale competition only
# = C if customer choice only
# = D if both wholesale competition and customer choice

# Create region variable corresponding to state abbreviation
data(state.regions)
names(state.regions)[2] <- "state"
state_reg2 <- join(state_reg2, state.regions, by = "state", type = "left")

# Map data
pdf("./Analysis/Graphics/Choropleth/restructuring.pdf", width = 10, height = 6)
map = StateChoropleth$new(state_reg2)
map$show_labels = FALSE
map$set_num_colors(1)
map$ggplot_scale = scale_fill_manual(values = c("A" = "#ffffcc",
                                                "B" = "#c2e699",
                                                "C" = "#78c679",
                                                "D" = "#238443"),
                                     labels = c("A" = "Vertically Integrated",
                                                "B" = "Wholesale Competition",
                                                "C" = "Customer Choice",
                                                "D" = "Wholesale Competition\nwith Customer Choice"),
                                     name = NULL)
map$render()
dev.off()


# Create choropleth maps by state and year for smart meter diffusion ####

# Prepare data for use by choroplethr package

# Select variables
meters2 <- meters[ , c("utility_id", "state", "year", "ami_total", "total_meters2")]

# Sum variables by state and year
meters2 <- ddply(meters2, .(state, year), numcolwise(sum))

# Create variable for smart meter proportion of metering stock per state
meters2$value <- meters2$ami_total / meters2$total_meters2
meters2$value <- round(meters2$value, digits = 2)

# Insepct data
summary(meters2$value)
# View(meters2)

# Create region variable corresponding to state abbreviation
data(state.regions)
names(state.regions)[2] <- "state"
meters2 <- join(meters2, state.regions, by = "state", type = "left")

# Create function to combine maps into one graph with common legend
gridArrangeSharedLegend <- function(..., ncol = length(list(...)), nrow = 1, position = c("bottom", "right")){
  plots <- list(...)
  position <- match.arg(position)
  g <- ggplotGrob(plots[[1]] + theme(legend.position = position))$grobs
  legend <- g[[which(sapply(g, function(x) x$name) == "guide-box")]]
  lheight <- sum(legend$height)
  lwidth <- sum(legend$width)
  gl <- lapply(plots, function(x) x + theme(legend.position="none"))
  gl <- c(gl, ncol = ncol, nrow = nrow)

  combined <- switch(position,
                     "bottom" = arrangeGrob(do.call(arrangeGrob, gl),
                                            legend,
                                            ncol = 1,
                                            heights = unit.c(unit(1, "npc") - lheight, lheight)),
                     "right" = arrangeGrob(do.call(arrangeGrob, gl),
                                           legend,
                                           ncol = 2,
                                           widths = unit.c(unit(1, "npc") - lwidth, lwidth)))
  grid.newpage()
  grid.draw(combined)
}

# 2007

# Select data for 2007
meters_2007 <- meters2[meters2$year == 2007, ]

# Map data
pdf("./Analysis/Graphics/Choropleth/ami_choropleth_2007.pdf", width = 10, height = 6)
map = StateChoropleth$new(meters_2007)
map$title = "2007"
map$show_labels = FALSE
map$set_num_colors(1)
map$ggplot_scale = scale_fill_gradient(limits = c(0, 1), low = "#fdfdfd", high = "#08519c",
                                       guide = "colorbar", name = "AMI\nProportion")
map_2007 <- map$render() + theme(plot.title = element_text(hjust = 0.5))
map$render() + theme(plot.title = element_text(hjust = 0.5))
dev.off()

# 2008

# Select data for 2008
meters_2008 <- meters2[meters2$year == 2008, ]

# Map data
pdf("./Analysis/Graphics/Choropleth/ami_choropleth_2008.pdf", width = 10, height = 6)
map = StateChoropleth$new(meters_2008)
map$title = "2008"
map$show_labels = FALSE
map$set_num_colors(1)
map$ggplot_scale = scale_fill_gradient(limits = c(0, 1), low = "#fdfdfd", high = "#08519c",
                                       guide = "colorbar", name = "AMI\nProportion")
map_2008 <- map$render() + theme(plot.title = element_text(hjust = 0.5))
map$render() + theme(plot.title = element_text(hjust = 0.5))
dev.off()

# 2009

# Select data for 2009
meters_2009 <- meters2[meters2$year == 2009, ]

# Map data
pdf("./Analysis/Graphics/Choropleth/ami_choropleth_2009.pdf", width = 10, height = 6)
map = StateChoropleth$new(meters_2009)
map$title = "2009"
map$show_labels = FALSE
map$set_num_colors(1)
map$ggplot_scale = scale_fill_gradient(limits = c(0, 1), low = "#fdfdfd", high = "#08519c",
                                       guide = "colorbar", name = "AMI\nProportion")
map_2009 <- map$render() + theme(plot.title = element_text(hjust = 0.5))
map$render() + theme(plot.title = element_text(hjust = 0.5))
dev.off()

# 2010

# Select data for 2010
meters_2010 <- meters2[meters2$year == 2010, ]

# Map data
pdf("./Analysis/Graphics/Choropleth/ami_choropleth_2010.pdf", width = 10, height = 6)
map = StateChoropleth$new(meters_2010)
map$title = "2010"
map$show_labels = FALSE
map$set_num_colors(1)
map$ggplot_scale = scale_fill_gradient(limits = c(0, 1), low = "#fdfdfd", high = "#08519c",
                                       guide = "colorbar", name = "AMI\nProportion")
map_2010 <- map$render() + theme(plot.title = element_text(hjust = 0.5))
map$render() + theme(plot.title = element_text(hjust = 0.5))
dev.off()

# 2011

# Select data for 2011
meters_2011 <- meters2[meters2$year == 2011, ]

# Map data
pdf("./Analysis/Graphics/Choropleth/ami_choropleth_2011.pdf", width = 10, height = 6)
map = StateChoropleth$new(meters_2011)
map$title = "2011"
map$show_labels = FALSE
map$set_num_colors(1)
map$ggplot_scale = scale_fill_gradient(limits = c(0, 1), low = "#fdfdfd", high = "#08519c",
                                       guide = "colorbar", name = "AMI\nProportion")
map_2011 <- map$render() + theme(plot.title = element_text(hjust = 0.5))
map$render() + theme(plot.title = element_text(hjust = 0.5))
dev.off()

# 2012

# Select data for 2012
meters_2012 <- meters2[meters2$year == 2012, ]

# Map data
pdf("./Analysis/Graphics/Choropleth/ami_choropleth_2012.pdf", width = 10, height = 6)
map = StateChoropleth$new(meters_2012)
map$title = "2012"
map$show_labels = FALSE
map$set_num_colors(1)
map$ggplot_scale = scale_fill_gradient(limits = c(0, 1), low = "#fdfdfd", high = "#08519c",
                                       guide = "colorbar", name = "AMI\nProportion")
map_2012 <- map$render() + theme(plot.title = element_text(hjust = 0.5))
map$render() + theme(plot.title = element_text(hjust = 0.5))
dev.off()

# 2013

# Select data for 2013
meters_2013 <- meters2[meters2$year == 2013, ]

# Map data
pdf("./Analysis/Graphics/Choropleth/ami_choropleth_2013.pdf", width = 10, height = 6)
map = StateChoropleth$new(meters_2013)
map$title = "2013"
map$show_labels = FALSE
map$set_num_colors(1)
map$ggplot_scale = scale_fill_gradient(limits = c(0, 1), low = "#fdfdfd", high = "#08519c",
                                       guide = "colorbar", name = "AMI\nProportion")
map_2013 <- map$render() + theme(plot.title = element_text(hjust = 0.5))
map$render() + theme(plot.title = element_text(hjust = 0.5))
dev.off()

# 2014

# Select data for 2014
meters_2014 <- meters2[meters2$year == 2014, ]

# Map data
pdf("./Analysis/Graphics/Choropleth/ami_choropleth_2014.pdf", width = 10, height = 6)
map = StateChoropleth$new(meters_2014)
map$title = "2014"
map$show_labels = FALSE
map$set_num_colors(1)
map$ggplot_scale = scale_fill_gradient(limits = c(0, 1), low = "#fdfdfd", high = "#08519c",
                                       guide = "colorbar", name = "AMI\nProportion")
map_2014 <- map$render() + theme(plot.title = element_text(hjust = 0.5))
map$render() + theme(plot.title = element_text(hjust = 0.5))
dev.off()

# Arrange maps on combined grid with shared legend
pdf("./Analysis/Graphics/Choropleth/ami_choropleths.pdf", width = 8.5, height = 11, onefile = FALSE)
gridArrangeSharedLegend(map_2007, map_2008, map_2009, map_2010,
                        map_2011, map_2012, map_2013, map_2014,
                        ncol = 2, nrow = 4, position = "right")
dev.off()


# Create choropleth maps by state and year for AMR diffusion ####

# Prepare data for use by choroplethr package

# Select variables
meters2 <- meters[ , c("utility_id", "state", "year", "amr_total", "total_meters2")]

# Sum variables by state and year
meters2 <- ddply(meters2, .(state, year), numcolwise(sum))

# Create variable for smart meter proportion of metering stock per state
meters2$value <- meters2$amr_total / meters2$total_meters2
meters2$value <- round(meters2$value, digits = 2)

# Insepct data
summary(meters2$value)
# View(meters2)

# Create region variable corresponding to state abbreviation
data(state.regions)
names(state.regions)[2] <- "state"
meters2 <- join(meters2, state.regions, by = "state", type = "left")

# 2007

# Select data for 2007
meters_2007 <- meters2[meters2$year == 2007, ]

# Map data
pdf("./Analysis/Graphics/Choropleth/amr_choropleth_2007.pdf", width = 10, height = 6)
map = StateChoropleth$new(meters_2007)
map$title = "2007"
map$show_labels = FALSE
map$set_num_colors(1)
map$ggplot_scale = scale_fill_gradient(limits = c(0, 1), low = "#fdfdfd", high = "#1c9099",
                                       guide = "colorbar", name = "AMR\nProportion")
map_2007 <- map$render() + theme(plot.title = element_text(hjust = 0.5))
map$render() + theme(plot.title = element_text(hjust = 0.5))
dev.off()

# 2008

# Select data for 2008
meters_2008 <- meters2[meters2$year == 2008, ]

# Map data
pdf("./Analysis/Graphics/Choropleth/amr_choropleth_2008.pdf", width = 10, height = 6)
map = StateChoropleth$new(meters_2008)
map$title = "2008"
map$show_labels = FALSE
map$set_num_colors(1)
map$ggplot_scale = scale_fill_gradient(limits = c(0, 1), low = "#fdfdfd", high = "#1c9099",
                                       guide = "colorbar", name = "AMR\nProportion")
map_2008 <- map$render() + theme(plot.title = element_text(hjust = 0.5))
map$render() + theme(plot.title = element_text(hjust = 0.5))
dev.off()

# 2009

# Select data for 2009
meters_2009 <- meters2[meters2$year == 2009, ]

# Map data
pdf("./Analysis/Graphics/Choropleth/amr_choropleth_2009.pdf", width = 10, height = 6)
map = StateChoropleth$new(meters_2009)
map$title = "2009"
map$show_labels = FALSE
map$set_num_colors(1)
map$ggplot_scale = scale_fill_gradient(limits = c(0, 1), low = "#fdfdfd", high = "#1c9099",
                                       guide = "colorbar", name = "AMR\nProportion")
map_2009 <- map$render() + theme(plot.title = element_text(hjust = 0.5))
map$render() + theme(plot.title = element_text(hjust = 0.5))
dev.off()

# 2010

# Select data for 2010
meters_2010 <- meters2[meters2$year == 2010, ]

# Map data
pdf("./Analysis/Graphics/Choropleth/amr_choropleth_2010.pdf", width = 10, height = 6)
map = StateChoropleth$new(meters_2010)
map$title = "2010"
map$show_labels = FALSE
map$set_num_colors(1)
map$ggplot_scale = scale_fill_gradient(limits = c(0, 1), low = "#fdfdfd", high = "#1c9099",
                                       guide = "colorbar", name = "AMR\nProportion")
map_2010 <- map$render() + theme(plot.title = element_text(hjust = 0.5))
map$render() + theme(plot.title = element_text(hjust = 0.5))
dev.off()

# 2011

# Select data for 2011
meters_2011 <- meters2[meters2$year == 2011, ]

# Map data
pdf("./Analysis/Graphics/Choropleth/amr_choropleth_2011.pdf", width = 10, height = 6)
map = StateChoropleth$new(meters_2011)
map$title = "2011"
map$show_labels = FALSE
map$set_num_colors(1)
map$ggplot_scale = scale_fill_gradient(limits = c(0, 1), low = "#fdfdfd", high = "#1c9099",
                                       guide = "colorbar", name = "AMR\nProportion")
map_2011 <- map$render() + theme(plot.title = element_text(hjust = 0.5))
map$render() + theme(plot.title = element_text(hjust = 0.5))
dev.off()

# 2012

# Select data for 2012
meters_2012 <- meters2[meters2$year == 2012, ]

# Map data
pdf("./Analysis/Graphics/Choropleth/amr_choropleth_2012.pdf", width = 10, height = 6)
map = StateChoropleth$new(meters_2012)
map$title = "2012"
map$show_labels = FALSE
map$set_num_colors(1)
map$ggplot_scale = scale_fill_gradient(limits = c(0, 1), low = "#fdfdfd", high = "#1c9099",
                                       guide = "colorbar", name = "AMR\nProportion")
map_2012 <- map$render() + theme(plot.title = element_text(hjust = 0.5))
map$render() + theme(plot.title = element_text(hjust = 0.5))
dev.off()

# 2013

# Select data for 2013
meters_2013 <- meters2[meters2$year == 2013, ]

# Map data
pdf("./Analysis/Graphics/Choropleth/amr_choropleth_2013.pdf", width = 10, height = 6)
map = StateChoropleth$new(meters_2013)
map$title = "2013"
map$show_labels = FALSE
map$set_num_colors(1)
map$ggplot_scale = scale_fill_gradient(limits = c(0, 1), low = "#fdfdfd", high = "#1c9099",
                                       guide = "colorbar", name = "AMR\nProportion")
map_2013 <- map$render() + theme(plot.title = element_text(hjust = 0.5))
map$render() + theme(plot.title = element_text(hjust = 0.5))
dev.off()

# 2014

# Select data for 2014
meters_2014 <- meters2[meters2$year == 2014, ]

# Map data
pdf("./Analysis/Graphics/Choropleth/amr_choropleth_2014.pdf", width = 10, height = 6)
map = StateChoropleth$new(meters_2014)
map$title = "2014"
map$show_labels = FALSE
map$set_num_colors(1)
map$ggplot_scale = scale_fill_gradient(limits = c(0, 1), low = "#fdfdfd", high = "#1c9099",
                                       guide = "colorbar", name = "AMR\nProportion")
map_2014 <- map$render() + theme(plot.title = element_text(hjust = 0.5))
map$render() + theme(plot.title = element_text(hjust = 0.5))
dev.off()

# Arrange maps on combined grid with shared legend
pdf("./Analysis/Graphics/Choropleth/amr_choropleths.pdf", width = 8.5, height = 11, onefile = FALSE)
gridArrangeSharedLegend(map_2007, map_2008, map_2009, map_2010,
                        map_2011, map_2012, map_2013, map_2014,
                        ncol = 2, nrow = 4, position = "right")
dev.off()
