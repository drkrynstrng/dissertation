# The Early Diffusion of Smart Meters in the US Electric Power Industry

This repository contains R programs for the data analysis in my doctoral dissertation, which studies the early diffusion of smart electricity meters in the US electric power industry. My dissertation and the data, R programs, and relevant R package versions can be accessed at https://osf.io/4fmuv/.


## Contents

Data - R programs for importing, merging, and cleaning data  
Analysis - R programs for exploring and analyzing cleaned data


## License

This project is licensed under the GNU GPLv3 license (see [LICENSE.txt](LICENSE.txt)).
